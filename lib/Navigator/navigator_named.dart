class Routes {
  static const SIGNUP_ROUTER = "SIGNUP_ROUTER";
  static const LOG_IN = "LOG_IN";
  static const ENTRO = "ENTRO";
  static const MAIN_ROUTER = "MAIN_ROUTER";
  static const MOVIE_DETAILS = "MOVIE_DETAILS";

}

abstract class NamedNavigator {
  Future push(String routeName,
      {dynamic arguments, bool replace = false, bool clean = false});

  void pop({dynamic result});
}