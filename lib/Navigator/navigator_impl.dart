import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app4/Ui/AllMovies/Components/all_movies_item.dart';
import 'package:flutter_app4/Ui/Auth/Login/login_page.dart';
import 'package:flutter_app4/Ui/Auth/SignUp/signup_page.dart';
import 'package:flutter_app4/Ui/Auth/entro_page.dart';
import 'file:///C:/Users/hp/AndroidStudioProjects/flutter_app4/lib/Ui/Main/main_page.dart';

import 'navigator_named.dart';

class NamedNavigatorImpl implements NamedNavigator {
  static final GlobalKey<NavigatorState> navigatorState =
  new GlobalKey<NavigatorState>();

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Routes.LOG_IN:
        return MaterialPageRoute(builder: (_) => Login());
      case Routes.SIGNUP_ROUTER:
        return MaterialPageRoute(builder: (_) => Signup());
        case Routes.ENTRO:
        return MaterialPageRoute(builder: (_) => Entro());
        case Routes.MAIN_ROUTER:
        return MaterialPageRoute(builder: (_) => MainPage());
      case Routes.MOVIE_DETAILS:
        return MaterialPageRoute(builder: (_) => MovieDetailsPage());
      // case Routes.SIGNUP_ROUTER:
      //   return MaterialPageRoute(builder: (_) => SignUp());

//      case Routes.REGISTER_FIRST_STEP_ROUTER:
//        return MaterialPageRoute(builder: (_) => RegisterFirstStep());
//      case Routes.REGISTER_SECOND_STEP_ROUTER:
//        return MaterialPageRoute(builder: (_) => RegisterSecondStep());
//      case Routes.REGISTER_THIRD_STEP_ROUTER:
//        return MaterialPageRoute(builder: (_) => RegisterThirdStep());
//      case Routes.HOME_ROUTER:
//        return MaterialPageRoute(builder: (_) => MainPage());
//      case Routes.MY_WALLET:
//        return MaterialPageRoute(builder: (_) => MyWallet());
//      case Routes.TERMS_AND_CONDITIONS:
//        return MaterialPageRoute(builder: (_) => TermsAndConditions());
//      case Routes.NOTIFICATIONS:
//        return MaterialPageRoute(builder: (_) => Notifications());
//      case Routes.PROFILE_SETTINGS:
//        return MaterialPageRoute(builder: (_) => ProfileSettings());
//      case Routes.UPDATE_ACADEMIC_DATA:
//        return MaterialPageRoute(builder: (_) => UpdateAcademicData());
//      case Routes.ITEM_DETAILS:
//        return MaterialPageRoute(
//            builder: (_) => ItemDetails(
//                  id: settings.arguments,
//                ));
//      case Routes.PDF_VIEWER:
//        return MaterialPageRoute(
//            builder: (_) => PdfView(
//                  pdfUrl: settings.arguments,
//                ));
//      case Routes.SUBJECT_COURSES:
//        return MaterialPageRoute(
//            builder: (_) => SubjectCourses(
//                  arguments: settings.arguments,
//                ));
//      case Routes.COURSE_DETAILS:
//        return MaterialPageRoute(
//            builder: (_) => CourseDetails(
//                  id: settings.arguments,
//                ));
//      case Routes.VIDEO_PREVIEW:
//        return MaterialPageRoute(
//            builder: (_) => VideoPreview(
//                  session: settings.arguments,
//                ));
//      case Routes.LIVE_STREAM:
//        return MaterialPageRoute(
//            builder: (_) => LiveStream(
//                  channelName: settings.arguments,
//                ));
//      case Routes.MY_COURSES_PAGE_ROUTER:
//        return MaterialPageRoute(builder: (_) => MyCoursesPage());
//        case Routes.MY_ITEMS_PAGE_ROUTER:
//        return MaterialPageRoute(builder: (_) => MyItemsPage());
//        case Routes.UPDATE_SUBJECTS:
//        return MaterialPageRoute(builder: (_) => UpdateSubjects());
//        case Routes.SEARCH:
//        return MaterialPageRoute(builder: (_) => Search());
//        case Routes.INTRO:
//        return MaterialPageRoute(builder: (_) => Intro());
//        case Routes.CONTENT_CREATOR:
//        return MaterialPageRoute(builder: (_) => ContentCreator());
//        case Routes.UPLOAD_ITEM:
//        return MaterialPageRoute(builder: (_) => UploadItem());
    }
    return MaterialPageRoute(builder: (_) => Container());
  }

  @override
  void pop({dynamic result}) {
    if (navigatorState.currentState.canPop())
      navigatorState.currentState.pop(result);
  }

  @override
  Future push(String routeName,
      {arguments, bool replace = false, bool clean = false}) {
    if (clean)
      return navigatorState.currentState.pushNamedAndRemoveUntil(
          routeName, (_) => false,
          arguments: arguments);

    if (replace)
      return navigatorState.currentState
          .pushReplacementNamed(routeName, arguments: arguments);

    return navigatorState.currentState
        .pushNamed(routeName, arguments: arguments);
  }
}