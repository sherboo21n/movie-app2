
import 'package:dio/dio.dart';
import 'package:flutter_app4/Response/Cast/cast.dart';
import 'package:flutter_app4/Response/Genres/genres.dart';
import 'file:///C:/Users/hp/AndroidStudioProjects/flutter_app4/lib/Response/Movies/movie.dart';
import 'package:flutter_app4/Response/MoviesDetails/movies_details.dart';

class MovieNetwork {
  static const baseUrl = "https://api.themoviedb.org/3/movie/";
  static const baseImagesUrl = "https://image.tmdb.org/t/p/";
  static const apiKey = "7b09cf0705817b4ab13f653c7d0c7b99";
  final Dio _dio = Dio();
  static const getGenreUrl = "https://api.themoviedb.org/3/genre/movie/list";
  static const getPersonUrl = "https://api.themoviedb.org/3/trending/person/week";
  static const getPersonDetails = "https://api.themoviedb.org/3/person/";




  Future<MovieResponse> getPlayingMovie() async{
    var params = {
      "api_key": apiKey,
      "language": "en_US",
      "page:": 1,
    };
    try{
      Response response = await _dio.get(baseUrl + "now_playing" , queryParameters: params);
      return MovieResponse.fromJson(response.data);
    }catch(error , stacktracer){
      print("Exception occures: $error stackTracer: $stacktracer");
      return MovieResponse.withError("$error");
    }
  }

  Future<MovieResponse> getAllMovies() async{
    var params = {
      "api_key": apiKey,
      "language": "en_US",
      "page:": 1,
    };
    try{
      Response response = await _dio.get(baseUrl + "now_playing" , queryParameters: params);
      return MovieResponse.fromJson(response.data);
    }catch(error , stacktracer){
      print("Exception occures: $error stackTracer: $stacktracer");
      return MovieResponse.withError("$error");
    }
  }

  Future<MovieResponse> getMovieInThisWeek() async{
    var params = {
      "api_key": apiKey,
      "language": "en_US",
      "page:": 1,
    };
    try{
      Response response = await _dio.get(baseUrl + "top_rated" , queryParameters: params);
      return MovieResponse.fromJson(response.data);
    }catch(error , stacktracer){
      print("Exception occures: $error stackTracer: $stacktracer");
      return MovieResponse.withError("$error");
    }
  }

  Future<MovieDetailsResponse> getMovieDetail(int id) async{
    var params = {
      "api_key": apiKey,
      "language": "en_US",
    };
    try{
      Response response = await _dio.get(baseUrl + "$id" , queryParameters: params);
      return MovieDetailsResponse.fromJson(response.data);
    } catch (error , stacktarace){
      print("Exception occures: $error stackTracer: $stacktarace");
      return MovieDetailsResponse.withError("$error");
    }
  }

  Future<GenreResponse> getGenres() async{
    var params = {
      "api_key": apiKey,
      "language": "en_US",
      "page:": 1,
    };
    try{
      Response response = await _dio.get(getGenreUrl, queryParameters: params);
      return GenreResponse.fromJson(response.data);
    }catch(error , stacktracer){
      print("Exception occures: $error stackTracer: $stacktracer");
      return GenreResponse.withError("$error");
    }
  }

  Future<CastResponse> getCasts(int id) async{
    var params = {
      "api_key": apiKey,
      "language": "en_US",
    };
    try{
      Response response = await _dio.get(baseUrl + "$id" + "/credits", queryParameters: params);
      return CastResponse.fromJson(response.data);
    } catch (error , stacktarace){
      print("Exception occures: $error stackTracer: $stacktarace");
      return CastResponse.withError("$error");
    }
  }


}