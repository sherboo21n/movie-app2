import 'package:flutter/material.dart';
import 'package:flutter_app4/Navigator/navigator_named.dart';
import 'Navigator/navigator_impl.dart';
import 'Theme/colors.dart';


void main()=> runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final GlobalKey<NavigatorState> navKey = GlobalKey();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      key: navKey,
      initialRoute: Routes.MAIN_ROUTER,
      title: "Kinema",
      onGenerateRoute: NamedNavigatorImpl.onGenerateRoute,
      navigatorKey: NamedNavigatorImpl.navigatorState,
      theme: ThemeData(
        appBarTheme: AppBarTheme(
          elevation: 0.0,
          color: Colors.transparent
        ),
        scaffoldBackgroundColor: Color(lightThemeColors['secondary']),
        // primaryColorBrightness: Brightness.dark,
        // brightness: Brightness.dark,
        primaryColor: Color(lightThemeColors['primary']),
        accentColor: Color(lightThemeColors['secondary']),
        hintColor: Color(lightThemeColors['text-head']),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}


