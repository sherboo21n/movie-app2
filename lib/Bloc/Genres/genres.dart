
import 'package:flutter_app4/Network/app_network.dart';
import 'package:flutter_app4/Response/Genres/genres.dart';
import 'package:rxdart/rxdart.dart';

class GenresListBloc{
  final MovieNetwork _repository = MovieNetwork();
  final BehaviorSubject<GenreResponse> _subject = BehaviorSubject<GenreResponse>();

  getGenres() async{
    GenreResponse response = await _repository.getGenres();
    _subject.sink.add(response);
  }

  dispose(){
    _subject.close();
  }

  BehaviorSubject<GenreResponse> get subject =>_subject;

}

final genresBloc = GenresListBloc();