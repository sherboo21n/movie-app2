import 'package:flutter/cupertino.dart';
import 'package:flutter_app4/Network/app_network.dart';
import 'package:flutter_app4/Response/Cast/cast.dart';
import 'package:rxdart/rxdart.dart';

class CastBloc{
  final MovieNetwork _repository = MovieNetwork();
  final BehaviorSubject<CastResponse> _subject = BehaviorSubject<CastResponse>();

  getCasts(int id) async{
    CastResponse response = await _repository.getCasts(id);
    _subject.sink.add(response);
  }


  void drainStream() {
    _subject.value = null;
  }
  @mustCallSuper

  void dispose() async{
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<CastResponse> get subject =>_subject;

}

final castBloc = CastBloc();