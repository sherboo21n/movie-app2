import 'package:flutter/material.dart';
import 'package:flutter_app4/Network/app_network.dart';
import 'package:flutter_app4/Response/MoviesDetails/movies_details.dart';
import 'package:rxdart/rxdart.dart';

class MovieDetailsBloc{
  final MovieNetwork _repository = MovieNetwork();
  final BehaviorSubject<MovieDetailsResponse> _subject = BehaviorSubject<MovieDetailsResponse>();

  getMovieDetails(int id) async{
    MovieDetailsResponse response = await _repository.getMovieDetail(id);
    _subject.sink.add(response);
  }


  void drainStream() {
    _subject.value = null;
  }
  @mustCallSuper

  void dispose() async{
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<MovieDetailsResponse> get subject =>_subject;

}

final movieDetailsBloc = MovieDetailsBloc();