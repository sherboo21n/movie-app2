import 'package:flutter_app4/Network/app_network.dart';
import 'file:///C:/Users/hp/AndroidStudioProjects/flutter_app4/lib/Response/Movies/movie.dart';
import 'package:rxdart/subjects.dart';

class AllMoviesBloc{
  final MovieNetwork _repository = MovieNetwork();
  final BehaviorSubject<MovieResponse> _subject = BehaviorSubject<MovieResponse>();

  getAllMovies() async{
    MovieResponse response = await _repository.getAllMovies();
    _subject.sink.add(response);
  }

  dispose(){
    _subject.close();
  }

  BehaviorSubject<MovieResponse> get subject =>_subject;

}

final allMoviesBloc = AllMoviesBloc();