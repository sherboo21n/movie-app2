import 'package:flutter_app4/Network/app_network.dart';
import 'file:///C:/Users/hp/AndroidStudioProjects/flutter_app4/lib/Response/Movies/movie.dart';
import 'package:rxdart/subjects.dart';

class NowPlayingListBloc{
  final MovieNetwork _repository = MovieNetwork();
  final BehaviorSubject<MovieResponse> _subject = BehaviorSubject<MovieResponse>();

  getPlayingMovies() async{
    MovieResponse response = await _repository.getPlayingMovie();
    _subject.sink.add(response);
  }

  dispose(){
    _subject.close();
  }

  BehaviorSubject<MovieResponse> get subject =>_subject;

}

final nowPlayingMoviesBloc = NowPlayingListBloc();