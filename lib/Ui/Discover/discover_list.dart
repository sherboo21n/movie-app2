import 'package:flutter/material.dart';
import 'package:flutter_app4/Bloc/AllMovies/all_movies.dart';
import 'file:///C:/Users/hp/AndroidStudioProjects/flutter_app4/lib/Response/Movies/movie.dart';
import 'package:flutter_app4/Ui/AllMovies/all_movies_list.dart';
import 'package:flutter_app4/Ui/Commen/dafault_error.dart';
import 'package:flutter_app4/Ui/Commen/dafault_loading.dart';
import 'package:flutter_app4/Ui/Commen/dafult_title.dart';
import 'package:flutter_app4/Ui/Discover/Components/discover_item.dart';
import 'package:flutter_app4/Ui/MoviInThisWeek/movie_in_this_week_list.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import '../NowPlaying/now_playing_list.dart';

class DiscoverPage extends StatefulWidget {
  @override
  _DiscoverPageState createState() => _DiscoverPageState();
}

class _DiscoverPageState extends State<DiscoverPage> {

  String error;

  @override
  void initState() {
    allMoviesBloc.getAllMovies();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Discover",
          style: TextStyle(
              color: Colors.white
          ),
        ),
          centerTitle: false,
        actions: [
          IconButton(
            onPressed: (){},
            icon: Icon(
              Icons.search,
              color: Colors.white,
            ),
          )
        ],
      ),
      body: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(
                top: 20
            ),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25),
                  topRight:  Radius.circular(25),
                )
            ),
          ),
          StreamBuilder<MovieResponse>(
              stream: allMoviesBloc.subject.stream,
              builder: (context , AsyncSnapshot<MovieResponse> snapshot){
                if(snapshot.hasData){
                  if(snapshot.data.error != null && snapshot.data.error.length > 0){
                    return DafaultError(
                        error: snapshot.data.error
                    );
                  }
                  return  DiscoverItem();
                } else if(snapshot.hasError){
                  return DafaultError(
                      error: snapshot.error
                  );
                } else {
                  return DafaultLoading();
                }
              }
          )
        ],
      )
    );
  }

}
