import 'package:flutter/material.dart';
import 'file:///C:/Users/hp/AndroidStudioProjects/flutter_app4/lib/Response/Movies/movie.dart';
import 'package:flutter_app4/Ui/AllMovies/all_movies_list.dart';
import 'package:flutter_app4/Ui/Commen/dafult_title.dart';
import 'package:flutter_app4/Ui/MoviInThisWeek/movie_in_this_week_list.dart';
import 'package:flutter_app4/Ui/NowPlaying/now_playing_list.dart';

class DiscoverItem extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return  Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(
          top: 30,
          left: 5,
          right: 5
      ),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(25),
            topRight:  Radius.circular(25),
          )
      ),
      child: ListView(
        shrinkWrap: true,
        children: [
          Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 20
                ),
                child: DafaultTitle(
                  title: "Now Playing",
                size: 16,
                ),
              )
          ),
          NowPlaying(),
          Padding(
              padding: const EdgeInsets.only(
                  left: 20,
                  top: 20,
                  bottom: 10
              ),
              child: DafaultTitle(
                title: "All Movies",
              size: 16
                )
          ),
          MovieInThisWeek(),
          Padding(
              padding: const EdgeInsets.only(
                  left: 20,
                  top: 5,
                  bottom: 12.5
              ),
              child: DafaultTitle(
                title: "Movies In  Week",
                size: 16,
              )
          ),
          AllMovies()
        ],
      ),
    );
  }
}
