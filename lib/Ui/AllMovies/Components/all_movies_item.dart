import 'file:///C:/Users/hp/AndroidStudioProjects/flutter_app4/lib/Model/Movies/movie.dart';
import 'file:///C:/Users/hp/AndroidStudioProjects/flutter_app4/lib/Response/Movies/movie.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app4/Bloc/MoviesDetails/movie_details.dart';
import 'package:flutter_app4/Response/MoviesDetails/movies_details.dart';
import 'package:flutter_app4/Ui/Commen/dafault_error.dart';
import 'package:flutter_app4/Ui/Commen/dafault_loading.dart';
import 'package:flutter_app4/Ui/Commen/dafult_title.dart';
import 'file:///C:/Users/hp/AndroidStudioProjects/flutter_app4/lib/Ui/MoviesDetails/Componants/Cast/movie_details_cast.dart';
import 'file:///C:/Users/hp/AndroidStudioProjects/flutter_app4/lib/Ui/MoviesDetails/Componants/Genre/movies_details_item.dart';


class AllMoviesItem extends StatelessWidget {

  MovieResponse data;
  AllMoviesItem({this.data});

  @override
  Widget build(BuildContext context) {
    return _buildNowPlaying(data , context);
  }

  Widget _buildNowPlaying(MovieResponse data , BuildContext context){
    List<Movie> movies = data.movies;
    if(movies.length == 0){
      return Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text('No movies')
          ],
        ),

      );
    } else {
      return Container(
        height: 220,
        child: ListView.builder(
            shrinkWrap: true,
            itemCount: movies.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) => Padding(
                  padding: const EdgeInsets.only(
                    left: 15,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context) =>MovieDetailsPage(
                            movie: movies[index],
                          )
                          )
                          );
                        },
                        child: Container(
                          height: 170,
                          width: 120,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.grey.shade200,
                            image: DecorationImage(
                              image: NetworkImage(
                                  "https://image.tmdb.org/t/p/w500" + movies[index].backPoster
                              ),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        movies[index].title,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 14
                        ),
                      ),
                      SizedBox(
                        height: 2.5,
                      ),
                      Text(
                        movies[index].releaseData,
                        style: TextStyle(
                            color: Colors.grey.shade400,
                            fontSize: 12
                        ),
                      ),
                    ],
                  ),
                )
        ),
      );
    }
  }
}

class MovieDetailsPage extends StatefulWidget {

  final Movie movie;
  MovieDetailsPage({this.movie});

  @override
  _MovieDetailsPageState createState() => _MovieDetailsPageState(movie);
}

class _MovieDetailsPageState extends State<MovieDetailsPage> {

  final Movie movie;
  _MovieDetailsPageState(this.movie);

  static const baseImagesUrl = "https://image.tmdb.org/t/p/";

  @override
  void initState() {
    // moviesVideoBloc..getMovieVideos(movie.id);
    movieDetailsBloc.getMovieDetails(movie.id);
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
        body:  StreamBuilder<MovieDetailsResponse>(
            stream: movieDetailsBloc.subject.stream,
            builder: (context , AsyncSnapshot<MovieDetailsResponse> snapshot){
              if(snapshot.hasData){
                if(snapshot.data.error != null && snapshot.data.error.length > 0){
                  return DafaultError(
                      error: snapshot.data.error
                  );
                }
                return  _build_Movie_Details_List(
                   snapshot.data,
                );
              } else if(snapshot.hasError){
                return DafaultError(
                    error: snapshot.error
                );
              } else {
                return DafaultLoading();
              }
            }
        )
    );
  }

  Widget _build_Movie_Details_List(MovieDetailsResponse data){
    return ListView(
      children: [
        Stack(
          children: [
            Container(
              height: MediaQuery.of(context).size.height/2.2,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(30),
                    bottomLeft:  Radius.circular(30),
                  ),
                  image: DecorationImage(
                      image: NetworkImage(
                        "${baseImagesUrl}w500" + movie.backPoster,
                      ),
                      fit: BoxFit.fill
                  )
              ),
            ),
            Positioned(
              top: 35,
              left: 8,
              child: IconButton(
                onPressed: (){
                  Navigator.pop(context);
                },
                icon: Icon(
                  Icons.arrow_back,
                  color: Colors.white,
                ),
              ),
            ),
            Positioned(
              top: 125,
              left: 0,
              right: 0,
              child: Center(
                child: IconButton(
                  onPressed: (){

                  },
                  icon: Icon(
                    Icons.play_circle_outline,
                    color: Colors.white,
                    size: 50,
                  ),
                ),
              ),
            ),
          ],
        ),
        Padding(
            padding: const EdgeInsets.only(
                top: 20,
                bottom: 5,
                left: 25,
                right: 25
            ),
            child: DafaultTitle(
              title: movie.title,
              size: 21,
            )
        ),
        MovieDetailsItem(
          id: movie.id,
        ),
        Padding(
            padding: const EdgeInsets.symmetric(
                vertical: 10,
                horizontal: 22.5
            ),
            child: DafaultTitle(
              title: "Story",
              size: 18,
            )
        ),
        Padding(
          padding: EdgeInsets.symmetric(
              horizontal: 22.5,
              vertical: 10
          ),
          child: Text(
            movie.overView,
            style: TextStyle(
              color: Colors.grey,
              fontSize: 14,
            ),
          ),
        ),
        Padding(
            padding: const EdgeInsets.symmetric(
                vertical: 10,
                horizontal: 22.5
            ),
            child: DafaultTitle(
              title: "Cast",
              size: 18,
            )
        ),
        MovieDetailsCast(
          id: movie.id,
        )
      ],
    );
  }
}