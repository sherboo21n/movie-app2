import 'package:flutter_app4/Bloc/AllMovies/all_movies.dart';
import 'file:///C:/Users/hp/AndroidStudioProjects/flutter_app4/lib/Response/Movies/movie.dart';
import 'package:flutter_app4/Ui/AllMovies/Components/all_movies_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app4/Ui/Commen/dafault_error.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class AllMovies extends StatefulWidget {
  @override
  _AllMoviesState createState() => _AllMoviesState();
}

class _AllMoviesState extends State<AllMovies> {

  @override
  void initState() {
    allMoviesBloc.getAllMovies();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<MovieResponse>(
        stream: allMoviesBloc.subject.stream,
        builder: (context , AsyncSnapshot<MovieResponse> snapshot){
          if(snapshot.hasData){
            if(snapshot.data.error != null && snapshot.data.error.length > 0){
              return DafaultError(
                  error: snapshot.data.error
              );
            }
            return AllMoviesItem(
              data: snapshot.data,
            );
          } else if(snapshot.hasError){
            return DafaultError(
                error: snapshot.error
            );
          } else {
            return Text("");
          }
        }
    );
  }
}