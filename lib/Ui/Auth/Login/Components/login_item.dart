import 'package:flutter/material.dart';
import 'package:flutter_app4/Navigator/navigator_impl.dart';
import 'package:flutter_app4/Navigator/navigator_named.dart';


class LoginItem extends StatefulWidget {

  @override
  _LoginItemState createState() => _LoginItemState();
}

class _LoginItemState extends State<LoginItem> {

  GlobalKey<FormState> _form_key = GlobalKey();
  bool _obscure = false;
  final int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _form_key,
      child:  Padding(
        padding: const EdgeInsets.symmetric(
            horizontal: 20
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextFormField(
              keyboardType: TextInputType.emailAddress,
              style: TextStyle(
                  color: Colors.grey
              ),
              decoration: InputDecoration(
                labelText: "E-mail",
                labelStyle: TextStyle(
                    color: Colors.white,
                    fontSize: 14
                ),
              ),
              validator: (String value) {
                if (value.isEmpty) {
                  "Please enter your e-mail";
                }
              },
            ),
            SizedBox(
              height: 15,
            ),
            TextFormField(
              keyboardType: TextInputType.visiblePassword,
              obscureText: _obscure ? false : true,
              style: TextStyle(
                  color: Colors.grey
              ),
              decoration: InputDecoration(
                  labelText: "Password",
                  labelStyle: TextStyle(
                      color: Colors.white,
                      fontSize: 14
                  ),
                  suffixIcon: InkWell(
                    onTap: (){
                      setState(() {
                        _obscure = !_obscure;
                      });
                    },
                    child: Icon(
                      _obscure ? Icons.visibility_off : Icons.visibility,
                      color: Colors.white,
                    ),
                  )
              ),
              validator: (String value) {
                if (value.length < 6) {
                  "Please enter your password";
                }
              },
            ),
            SizedBox(
              height: 50,
            ),
            Text(
              "Forget password?",
              style: TextStyle(
                  color: Colors.grey,
                  fontSize: 14
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Padding(
                padding: EdgeInsets.only(
                    top: 40
                ),
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: 2,
                  itemBuilder: (ctx , index) =>InkWell(
                    onTap: (){
                      setState(() {
                        index = _selectedIndex;
                      });
                    },
                    child: Column(
                      children: [
                        InkWell(
                          onTap: (){
                            var mNamedNavigator = NamedNavigatorImpl();
                            mNamedNavigator.push(
                                _selectedIndex == index ? Routes.MAIN_ROUTER : Routes.SIGNUP_ROUTER,
                                replace: false,
                                clean: false);
                            if (!_form_key.currentState.validate()) {
                              return;
                            }
                          },
                          child: Container(
                            height: 45,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                color: _selectedIndex == index ? Theme.of(context).primaryColor : Colors.transparent,
                                border: Border.all(
                                    color: _selectedIndex == index ? Colors.transparent : Colors.white
                                ),
                                borderRadius: BorderRadius.circular(30)
                            ),
                            child: Center(
                              child: Text(
                                _selectedIndex == index ? "Sign IN" :  "Sign Up",
                                style: TextStyle(
                                    color: _selectedIndex == index ? Colors.black : Colors.white
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),

                      ],
                    ),
                  ),
                )
            )
          ],
        ),
      ),
    );
  }
}
