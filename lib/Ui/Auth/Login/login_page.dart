import 'package:flutter/material.dart';
import 'package:flutter_app4/Navigator/navigator_impl.dart';
import 'package:flutter_app4/Navigator/navigator_named.dart';
import 'package:flutter_app4/Ui/Auth/Login/Components/login_item.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Container(
        color: Colors.black45,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: ListView(
          shrinkWrap: true,
          children: [
            SizedBox(
              height: 100,
            ),
            Center(
              child: Text(
                "SIGN IN",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 32,
                    fontWeight: FontWeight.w500
                ),
              ),
            ),
            SizedBox(
              height: 120,
            ),
            LoginItem()
          ],
        ),
      ),
    );
  }
}
