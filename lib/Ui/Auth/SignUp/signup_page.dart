import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'Components/signup_item.dart';

class Signup extends StatefulWidget {
  @override
  _SignupState createState() => _SignupState();
}

class _SignupState extends State<Signup> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Container(
        color: Colors.black45,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: ListView(
          shrinkWrap: true,
          children: [
            SizedBox(
              height: 80,
            ),
            Center(
              child: Text(
                "SIGN IN",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 32,
                    fontWeight: FontWeight.w500
                ),
              ),
            ),
            SizedBox(
              height: 70,
            ),
            SignupItem()
          ],
        ),
      ),
    );
  }
}