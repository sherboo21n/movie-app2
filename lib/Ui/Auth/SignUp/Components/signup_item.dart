import 'package:flutter/material.dart';
import 'package:flutter_app4/Navigator/navigator_impl.dart';
import 'package:flutter_app4/Navigator/navigator_named.dart';


class SignupItem extends StatefulWidget {

  @override
  _SignupItemState createState() => _SignupItemState();
}

class _SignupItemState extends State<SignupItem> {

  GlobalKey<FormState> _form_key = GlobalKey();
  bool _obscure = false;
  final int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _form_key,
      child:  Padding(
        padding: const EdgeInsets.symmetric(
            horizontal: 20
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextFormField(
              keyboardType: TextInputType.name,
              style: TextStyle(
                  color: Colors.grey
              ),
              decoration: InputDecoration(
                labelText: "Name",
                labelStyle: TextStyle(
                    color: Colors.white,
                    fontSize: 14
                ),
              ),
              validator: (String value) {
                if (value.isEmpty || value == null) {
                  "Please enter your name";
                }
              },
            ),
            SizedBox(
              height: 15,
            ),
            TextFormField(
              keyboardType: TextInputType.emailAddress,
              style: TextStyle(
                  color: Colors.grey
              ),
              decoration: InputDecoration(
                labelText: "E-mail",
                labelStyle: TextStyle(
                    color: Colors.white,
                    fontSize: 14
                ),
              ),
              validator: (String value) {
                if (value.isEmpty || value == null) {
                  "Please enter your e-mail";
                }
              },
            ),
            SizedBox(
              height: 15,
            ),
            TextFormField(
              keyboardType: TextInputType.visiblePassword,
              obscureText: _obscure ? false : true,
              style: TextStyle(
                  color: Colors.grey
              ),
              decoration: InputDecoration(
                  labelText: "Password",
                  labelStyle: TextStyle(
                      color: Colors.white,
                      fontSize: 14
                  ),
                  suffixIcon: InkWell(
                    onTap: (){
                      setState(() {
                        _obscure = !_obscure;
                      });
                    },
                    child: Icon(
                      _obscure ? Icons.visibility_off : Icons.visibility,
                      color: Colors.white,
                    ),
                  )
              ),
              validator: (String value) {
                if (value.length < 6 || value == null) {
                  "Please enter your password";
                }
              },
            ),
            SizedBox(
              height: 15,
            ),
            TextFormField(
              keyboardType: TextInputType.visiblePassword,
              obscureText: _obscure ? false : true,
              style: TextStyle(
                  color: Colors.grey
              ),
              decoration: InputDecoration(
                  labelText: "Re-typePassword",
                  labelStyle: TextStyle(
                      color: Colors.white,
                      fontSize: 14
                  ),
                  suffixIcon: InkWell(
                    onTap: (){
                      setState(() {
                        _obscure = !_obscure;
                      });
                    },
                    child: Icon(
                      _obscure ? Icons.visibility_off : Icons.visibility,
                      color: Colors.white,
                    ),
                  )
              ),
              validator: (String value) {
                if (value.length < 6 || value == null) {
                  "The password isnt match";
                }
              },
            ),
            SizedBox(
              height: 30,
            ),
            Padding(
                padding: EdgeInsets.only(
                    top: 40
                ),
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: 2,
                  itemBuilder: (ctx , index) =>InkWell(
                    onTap: (){
                      setState(() {
                        index = _selectedIndex;
                      });
                    },
                    child: Column(
                      children: [
                        InkWell(
                          onTap: (){
                            if (!_form_key.currentState.validate()) {
                              return;
                            }
                            var mNamedNavigator = NamedNavigatorImpl();
                            mNamedNavigator.push(
                                _selectedIndex == index ? Routes.MAIN_ROUTER : Routes.LOG_IN,
                                replace: false,
                                clean: false);
                          },
                          child: Container(
                            height: 45,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                color: _selectedIndex == index ? Theme.of(context).primaryColor : Colors.transparent,
                                border: Border.all(
                                    color: _selectedIndex == index ? Colors.transparent : Colors.white
                                ),
                                borderRadius: BorderRadius.circular(30)
                            ),
                            child: Center(
                              child: Text(
                                _selectedIndex == index ? "Sign Up" :  "Sign In",
                                style: TextStyle(
                                    color: _selectedIndex == index ? Colors.black : Colors.white
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),

                      ],
                    ),
                  ),
                )
            )
          ],
        ),
      ),
    );
  }
}
