import 'package:flutter/material.dart';
import 'package:flutter_app4/Navigator/navigator_impl.dart';
import 'package:flutter_app4/Navigator/navigator_named.dart';

class Entro extends StatefulWidget {
  @override
  _EntroState createState() => _EntroState();
}

class _EntroState extends State<Entro> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Image.asset(
            "assets/images/cinema.jpg",
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.fill,
          ),
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Colors.black54,
          ),
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Colors.black26,
          ),
          Positioned(
            top: 170,
            left: 0,
            right: 0,
            child: Center(
              child: Text(
                "CINEMA",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 32,
                  fontWeight: FontWeight.w600
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 20,
            left: 45,
            right: 45,
            child:  Center(
              child: Column(
                children: [
                  InkWell(
                    onTap: (){
                      var mNamedNavigator = NamedNavigatorImpl();
                      mNamedNavigator.push(Routes.LOG_IN,
                          replace: false,
                          clean: false);
                    },
                    child: Container(
                      height: 45,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          color: Colors.transparent,
                          border: Border.all(
                              color: Colors.white
                          ),
                          borderRadius: BorderRadius.circular(30)
                      ),
                      child: Center(
                        child: Text(
                          "Sign In",
                          style: TextStyle(
                              color: Colors.white
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  InkWell(
                    onTap: (){
                      var mNamedNavigator = NamedNavigatorImpl();
                      mNamedNavigator.push(Routes.SIGNUP_ROUTER,
                          replace: false,
                          clean: false);
                    },
                    child: Container(
                      height: 45,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.circular(30)
                      ),
                      child: Center(
                        child: Text(
                          "Sign Up",
                          style: TextStyle(
                              color: Colors.black
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      )
    );
  }
}
