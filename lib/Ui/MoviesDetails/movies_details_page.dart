import 'package:flutter/material.dart';
import 'package:flutter_app4/Model/Movies/movie.dart';
import 'package:flutter_app4/Ui/Commen/dafult_title.dart';

import 'Componants/Genre/movies_details_item.dart';


class MovieDetailsPage extends StatefulWidget {

  final Movie movie;
  MovieDetailsPage({this.movie});

  @override
  _MovieDetailsPageState createState() => _MovieDetailsPageState(movie);
}

class _MovieDetailsPageState extends State<MovieDetailsPage> {

  final Movie movie;
  _MovieDetailsPageState(this.movie);

  static const baseImagesUrl = "https://image.tmdb.org/t/p/";

  @override
  void initState() {
    // moviesVideoBloc..getMovieVideos(movie.id);
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body:  ListView(
          children: [
            Stack(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height/2.2,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(30),
                        bottomLeft:  Radius.circular(30),
                      ),
                      image: DecorationImage(
                          image: NetworkImage(
                            "${baseImagesUrl}w500" + widget.movie.backPoster,
                          ),
                          fit: BoxFit.fill
                      )
                  ),
                ),
                Positioned(
                  top: 35,
                  left: 8,
                  child: IconButton(
                    onPressed: (){
                      Navigator.pop(context);
                    },
                    icon: Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                    ),
                  ),
                ),
                Positioned(
                  top: 125,
                  left: 0,
                  right: 0,
                  child: Center(
                    child: IconButton(
                      onPressed: (){

                      },
                      icon: Icon(
                        Icons.play_circle_outline,
                        color: Colors.white,
                        size: 50,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Padding(
                padding: const EdgeInsets.only(
                    top: 20,
                    bottom: 5,
                    left: 25,
                    right: 25
                ),
                child: DafaultTitle(
                  title: movie.title,
                  size: 21,
                )
            ),
            MovieDetailsItem(
              id: movie.id,
            ),
            Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 10,
                    horizontal: 22.5
                ),
                child: DafaultTitle(
                  title: "Story",
                  size: 18,
                )
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 22.5,
                  vertical: 10
              ),
              child: Text(
                movie.overView,
                style: TextStyle(
                  color: Colors.grey,
                  fontSize: 14,
                ),
              ),
            )
          ],
        )
    );
  }
}