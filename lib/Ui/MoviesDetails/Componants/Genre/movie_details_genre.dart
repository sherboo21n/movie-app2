import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app4/Model/Movies/movie.dart';
import 'package:flutter_app4/Model/Movies_Details/movie_details.dart';
import 'package:flutter_app4/Response/MoviesDetails/movies_details.dart';

class MovieDetailsGenre extends StatelessWidget {

  final MovieDetailsResponse data;
  final Movie movie;
  MovieDetailsGenre({this.data , this.movie});

  @override
  Widget build(BuildContext context) {
    MovieDetails details = data.movieDetails;

    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
              children: details.genres.map((g) => Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  FilterChip(
                      backgroundColor: Colors.grey.shade300,
                      label: Text(
                          g.name
                      ) ,
                      labelStyle: TextStyle(
                          fontSize: 10,
                          fontWeight: FontWeight.bold,
                          color: Colors.grey.shade500
                      ),
                      onSelected: (b){}
                  ),
                  SizedBox(
                    width: 5,
                  )
                ],
              )
              ).toList()
          ),

        ],
      ),
    );
  }
}
