import 'package:flutter/material.dart';
import 'package:flutter_app4/Bloc/MoviesDetails/movie_details.dart';
import 'package:flutter_app4/Model/Movies_Details/movie_details.dart';
import 'package:flutter_app4/Response/MoviesDetails/movies_details.dart';
import 'package:flutter_app4/Ui/Commen/dafault_error.dart';
import 'file:///C:/Users/hp/AndroidStudioProjects/flutter_app4/lib/Ui/MoviesDetails/Componants/Genre/movie_details_genre.dart';

class MovieDetailsItem extends StatefulWidget {

  final int id;
  MovieDetailsItem({this.id});

  @override
  _MovieDetailsItemState createState() => _MovieDetailsItemState(id);
}

class _MovieDetailsItemState extends State<MovieDetailsItem> {

  final int id;
  _MovieDetailsItemState(this.id);


  @override
  void initState() {
    movieDetailsBloc..getMovieDetails(id);
    super.initState();
  }

  @override
  void dispose() {
    movieDetailsBloc..drainStream();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<MovieDetailsResponse>(
        stream: movieDetailsBloc.subject.stream,
        builder: (context, AsyncSnapshot<MovieDetailsResponse> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.error != null && snapshot.data.error.length > 0) {
              return DafaultError(
                  error: snapshot.data.error
              );
            }
            return MovieDetailsGenre(
                data: snapshot.data,
            );
          } else if (snapshot.hasError) {
            return DafaultError(
                error: snapshot.error
            );
          } else {
            return Text(
              ""
            );
          }
        }
    );
  }

}