import 'package:flutter/material.dart';
import 'package:flutter_app4/Bloc/Cast/cast.dart';
import 'package:flutter_app4/Model/Cast/cast.dart';
import 'package:flutter_app4/Response/Cast/cast.dart';
import 'package:flutter_app4/Ui/Commen/dafault_error.dart';

class MovieDetailsCast extends StatefulWidget {

  final int id;
  MovieDetailsCast({this.id});

  @override
  _MovieDetailsCastState createState() => _MovieDetailsCastState(id);
}

class _MovieDetailsCastState extends State<MovieDetailsCast> {

  final int id;
  _MovieDetailsCastState(this.id);


  @override
  void initState() {
    castBloc..getCasts(id);
    super.initState();
  }

  @override
  void dispose() {
    castBloc..drainStream();
    super.dispose();
  }
  static const baseImagesUrl = "https://image.tmdb.org/t/p/";

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<CastResponse>(
        stream: castBloc.subject.stream,
        builder: (context, AsyncSnapshot<CastResponse> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.error != null && snapshot.data.error.length > 0) {
              return DafaultError(
                  error: snapshot.data.error
              );
            }
            return  _build_Cast(snapshot.data);
          } else if (snapshot.hasError) {
            return DafaultError(
                error: snapshot.error
            );
          } else {
            return Text(
                ""
            );
          }
        }
    );
  }
 Widget _build_Cast(CastResponse data){
   List<Cast> casts = data.casts;
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 10
      ),
      child: Container(
        height: 220,
        child: ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: casts.length,
          itemBuilder: (context , index) => Padding(
            padding: const EdgeInsets.only(
                left: 10
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 140,
                  width: 130,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.grey.shade200,
                    image: DecorationImage(
                      image: NetworkImage(
                       " ${baseImagesUrl}w500${casts[index].image}"
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),

                SizedBox(
                  height: 10,
                ),
                Text(
                  casts[index].name,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 14
                  ),
                ),
                SizedBox(
                  height: 2.5,
                ),
                Text(
                  casts[index].character,
                  style: TextStyle(
                      color: Colors.grey.shade400,
                      fontSize: 12
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
 }
}