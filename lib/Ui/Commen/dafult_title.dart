import 'package:flutter/material.dart';

class DafaultTitle extends StatelessWidget {

  final String title;
  final double size;
  DafaultTitle({this.title , this.size });

  @override
  Widget build(BuildContext context) {
    return  Text(
      title,
      style: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.w500,
          fontSize: size
      ),
    );

  }
}
