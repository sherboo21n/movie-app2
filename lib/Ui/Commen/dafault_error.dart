import 'package:flutter/material.dart';

class DafaultError extends StatelessWidget {

  final String error;
  DafaultError({this.error});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Error Occured: $error')
        ],
      ),
    );
  }
}
