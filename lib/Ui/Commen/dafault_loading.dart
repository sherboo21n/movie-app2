import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class DafaultLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
          padding: EdgeInsets.only(
              top: 10
          ),
          child: SpinKitThreeBounce(
            color: Theme.of(context).accentColor,
            size: 30.0,
          )
      ),
    );
  }
}
