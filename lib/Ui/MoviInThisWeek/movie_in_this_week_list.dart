
import 'package:flutter/material.dart';
import 'package:flutter_app4/Bloc/MoviesInThisWeek/movies_in_this_week.dart';
import 'package:flutter_app4/Ui/AllMovies/Components/all_movies_item.dart';
import 'file:///C:/Users/hp/AndroidStudioProjects/flutter_app4/lib/Model/Movies/movie.dart';
import 'package:flutter_app4/Ui/Commen/dafault_error.dart';
import 'file:///C:/Users/hp/AndroidStudioProjects/flutter_app4/lib/Response/Movies/movie.dart';

class MovieInThisWeek extends StatefulWidget {
  @override
  _MovieInThisWeekState createState() => _MovieInThisWeekState();
}

class _MovieInThisWeekState extends State<MovieInThisWeek> {

  @override
  void initState() {
    movieInThisWeekBloc.getMoviesInThisWeek();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 5,),
        StreamBuilder<MovieResponse>(
            stream: movieInThisWeekBloc.subject.stream,
            builder: (context, AsyncSnapshot<MovieResponse> snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data.error != null &&
                    snapshot.data.error.length > 0) {
                  return DafaultError(
                      error: snapshot.data.error
                  );
                }
                return _buildTopRatedMovies( snapshot.data);
              } else if (snapshot.hasError) {
                return DafaultError(
                    error: snapshot.error
                );
              } else {
                return Text("");
              }
            }
        )
      ],
    );
  }


  Widget _buildTopRatedMovies (MovieResponse data){
    List<Movie> movies = data.movies;
    if(movies.length ==0){
      return Container(
        child: Text("No Movies"),
      );
    } else {
      return Container(
        height: 220,
        child: ListView.builder(
            shrinkWrap: true,
            itemCount: movies.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) =>
                Padding(
                  padding: const EdgeInsets.only(
                    left: 15,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context) =>MovieDetailsPage(
                            movie: movies[index],
                          )
                          )
                          );
                        },
                        child: Container(
                          height: 170,
                          width: 120,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.grey.shade200,
                            image: DecorationImage(
                              image: NetworkImage(
                                  "https://image.tmdb.org/t/p/w500" + movies[index].backPoster
                              ),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        movies[index].title,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 14
                        ),
                      ),
                      SizedBox(
                        height: 2.5,
                      ),
                      Text(
                        movies[index].releaseData,
                        style: TextStyle(
                            color: Colors.grey.shade400,
                            fontSize: 12
                        ),
                      ),
                    ],
                  ),
                )
        ),
      );
    }
  }

}
