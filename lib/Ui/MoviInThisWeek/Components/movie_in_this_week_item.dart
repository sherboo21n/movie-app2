import 'package:flutter/material.dart';
import 'file:///C:/Users/hp/AndroidStudioProjects/flutter_app4/lib/Model/Movies/movie.dart';
import 'package:flutter_app4/Response/Movies/movie.dart';

class MovieInThisWeekItem extends StatelessWidget {

  MovieResponse data;
  MovieInThisWeekItem({this.data});

  @override
  Widget build(BuildContext context) {
    return _buildMovieInThisWeek(data, context);
  }

  Widget _buildMovieInThisWeek (MovieResponse data , BuildContext context){
    List<Movie> movies = data.movies;
    if(movies.length ==0){
      return Container(
        child: Text("No Movies"),
      );
    } else {
      return Container(
        height: 220,
        child: ListView.builder(
            shrinkWrap: true,
            itemCount: movies.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) =>
                Padding(
                  padding: const EdgeInsets.only(
                    left: 15,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 170,
                        width: 120,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.grey.shade200,
                          image: DecorationImage(
                            image: NetworkImage(
                                "https://image.tmdb.org/t/p/w500" + movies[index].backPoster
                            ),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        movies[index].title,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 14
                        ),
                      ),
                      SizedBox(
                        height: 2.5,
                      ),
                      Text(
                        movies[index].releaseData,
                        style: TextStyle(
                            color: Colors.grey.shade400,
                            fontSize: 12
                        ),
                      ),
                    ],
                  ),
                )
        ),
      );
    }
  }
}
