import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'file:///C:/Users/hp/AndroidStudioProjects/flutter_app4/lib/Model/Movies/movie.dart';
import 'file:///C:/Users/hp/AndroidStudioProjects/flutter_app4/lib/Response/Movies/movie.dart';
import 'package:flutter_app4/Ui/AllMovies/Components/all_movies_item.dart';

class NowPlayingItem extends StatelessWidget {

  MovieResponse data;
  NowPlayingItem({this.data});

  @override
  Widget build(BuildContext context) {
    return _buildNowPlaying(data , context);
  }

  Widget _buildNowPlaying(MovieResponse data , BuildContext context){
    List<Movie> movies = data.movies;
    if(movies.length == 0){
      return Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text('No movies')
          ],
        ),

      );
    } else {
      return CarouselSlider(
        options: CarouselOptions(
          height: 360,
          enlargeCenterPage: true,
          viewportFraction: 0.65,
          autoPlay: true,
        ),
        items: movies.map((m) => Builder(
            builder: (BuildContext context) => InkWell(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) =>MovieDetailsPage(
                  movie: m,
                )
                )
                );
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.grey.shade200,
                  image: DecorationImage(
                    image: NetworkImage(
                        "https://image.tmdb.org/t/p/w500" + m.backPoster
                    ),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            )
        )
        ).toList(),
      );
    }
  }
}
