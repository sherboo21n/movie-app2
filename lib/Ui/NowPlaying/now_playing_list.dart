import 'file:///C:/Users/hp/AndroidStudioProjects/flutter_app4/lib/Bloc/NowPlaying/now_playing.dart';
import 'file:///C:/Users/hp/AndroidStudioProjects/flutter_app4/lib/Response/Movies/movie.dart';
import 'package:flutter_app4/Ui/Commen/dafault_error.dart';
import 'package:flutter_app4/Ui/NowPlaying/Components/now_playing_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class NowPlaying extends StatefulWidget {
  @override
  _NowPlayingState createState() => _NowPlayingState();
}

class _NowPlayingState extends State<NowPlaying> {

  @override
  void initState() {
    nowPlayingMoviesBloc.getPlayingMovies();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<MovieResponse>(
        stream: nowPlayingMoviesBloc.subject.stream,
        builder: (context , AsyncSnapshot<MovieResponse> snapshot){
          if(snapshot.hasData){
            if(snapshot.data.error != null && snapshot.data.error.length > 0){
              return DafaultError(
                  error: snapshot.data.error
              );
            }
            return NowPlayingItem(
              data: snapshot.data,
            );
          } else if(snapshot.hasError){
            return DafaultError(
                error: snapshot.error
            );
          } else {
            return Text("");
          }
        }
    );
  }
}
