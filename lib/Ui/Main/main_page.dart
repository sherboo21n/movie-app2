import 'package:flutter/material.dart';

import '../Discover/discover_list.dart';


class MainPage extends StatefulWidget {

  final int index;
  final String  branchId  ;

  const MainPage({Key key, this.index, this.branchId}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {

  var _pages = [];
  int _selectedIndex = 0;
  bool _selectedIndex2 = false;

  whichPage() {
    if (widget.index == null) {
      setState(() {
        _selectedIndex = 0;
      });
    } else {
      setState(() {
        _selectedIndex = widget.index;
      });
    }
  }

  @override
  void initState() {
    _pages = [
     DiscoverPage(),
      Text(
       "xgfxgd"
     ),
      Text(
       "lklkjh"
     ),
      Text(
       "kgjhk"
     ),
    ];
    whichPage();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.white,
          elevation: 5,
          type: BottomNavigationBarType.fixed,
          currentIndex: _selectedIndex,
          selectedItemColor: Theme.of(context).primaryColor,
          unselectedItemColor: Colors.grey.shade400,
          items: [
            BottomNavigationBarItem(
            icon: Icon(
              Icons.explore
                ),
              title: Text(
                "Discover",
                style: TextStyle(
                  color:   Colors.black,
                  fontSize: 14
                ),
              )
            ),
            BottomNavigationBarItem(
            icon: Icon(
              Icons.play_circle_outline
                ),
              title: Text(
                "Upcoming",
                style: TextStyle(
                    color:  Colors.black,
                    fontSize: 14
                ),
              )
            ),
            BottomNavigationBarItem(
            icon: Icon(
              Icons.access_time
                ),
              title: Text(
                "Watchlist",
                style: TextStyle(
                    color:   Colors.black,
                    fontSize: 14
                ),
              )
            ),
            BottomNavigationBarItem(
            icon: Icon(
              Icons.person
                ),
              title: Text(
                "Profile",
                style: TextStyle(
                    color:   Colors.black,
                    fontSize: 14
                ),
              )
            ),
          ],
          onTap: (index) {
            setState(() {
              _selectedIndex  = index;
              _selectedIndex2 = !_selectedIndex2;
            });
          },
        ),
        body: _pages[_selectedIndex]
    );
  }
}
